"per the Vundle git page
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"plugin commands go here

"better c++ syntax
Plugin 'octol/vim-cpp-enhanced-highlight'

"indent guides
"Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'Yggdroot/indentLine'

"tree view
Plugin 'scrooloose/nerdtree'
:command Tree NERDTreeToggle

"Syntax
Plugin 'vim-syntastic/syntastic'

call vundle#end()

"resume normal config
set encoding=utf-8
set fileencoding=utf-8
"attempt to intelligently autoindent based on file type
filetype indent plugin on
highlight clear
"enable syntax highlighting
syntax on
"command line completion
set wildmenu
set showcmd

"allow backspacing over autoindent, line break, start of insert
set backspace=indent,eol,start

"if file based indent fails, this keeps the indentation of the previous line
set autoindent

"always display satus line
set laststatus=2

"asks to save if command needs a saved file
set confirm

"enables the use of mouse
set mouse=a

"displays line numbers
set number

"useful remaps
"maps jj to esc
inoremap jj <Esc>l
:command W w
:command Q q
:command WQ wq
nnoremap ; :
vnoremap ; :

"removes annoying bells
set noerrorbells
set novisualbell
set t_vb=
set tm=500

"tab settings
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

"sets spacing to 2 instead of 4 for xml files
autocmd FileType xsd setlocal tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType xml setlocal tabstop=2 shiftwidth=2 softtabstop=2

"stops auto commenting new lines
"autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"searching
set incsearch   "searches as characters are typed
set hlsearch    "highlights search results
nnoremap <leader><space> :nohlsearch<CR>  
"remaps \space to turning off old search result highlight

set visualbell "blinks instead of beeping

"visually shows tabs and newlines
set listchars=tab:▸\ ,eol:¬

"stops vim from writing backup files
set nobackup
